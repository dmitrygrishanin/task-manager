package com.tasks.app;

import com.tasks.app.db.TaskDAO;

import com.tasks.app.resources.TaskResource;
import io.dropwizard.Application;

import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.jdbi.v3.core.Jdbi;


public class TaskManagerApplication extends Application<Configuration> {
    public static void main(final String[] args) throws Exception {

        new TaskManagerApplication().run(args);
    }

    @Override
    public String getName() {
        return "taskmanager";
    }

    @Override
    public void initialize(final Bootstrap<Configuration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final Configuration configuration,
                    final Environment environment) {
        final JdbiFactory factory = new JdbiFactory();
        final Jdbi jdbi = factory.build(environment, configuration.getDataSourceFactory(), "postgresql");
        final TaskDAO dao = jdbi.onDemand(TaskDAO.class);
        environment.jersey().register(new TaskResource(dao));

    }

}
