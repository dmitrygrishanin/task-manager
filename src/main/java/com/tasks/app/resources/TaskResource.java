package com.tasks.app.resources;

import com.tasks.app.db.TaskDAO;
import com.tasks.app.entity.Task;
import io.dropwizard.hibernate.UnitOfWork;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Path("/tasks")
public class TaskResource  {

    private TaskDAO taskDAO;

    public TaskResource(TaskDAO taskDAO) {
        this.taskDAO = taskDAO;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public List<Task> listOfTasks() {
        return taskDAO.getAllTasks();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Optional<Task> getTaskById(@PathParam("id") String id) {
        return taskDAO.findTaskById(id);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public void insertTask(Task task) {
        task.setId(UUID.randomUUID().toString());
        taskDAO.insertTask(task);
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Optional<Task> updateTaskById(@PathParam("id") String id, Task task) {
        taskDAO.updateTask(task, id);
        return taskDAO.findTaskById(id);
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public String deleteTaskById(@PathParam("id") String id) {
        taskDAO.deleteTask(id);
        return "Task is removed successfully!";
    }
}
